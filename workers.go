package main

// Запуск каналов
func (c *Config) startChans() {
	c.ch.cronJobs = make(chan ForWorker, 200)
	c.ch.parseJobs = make(chan Element, 100)
	c.ch.fetchJobs = make(chan FromParse, 200)
}

// Запуск воркеров
func (c *Config) startWorkers() {

	for i := 0; i < 200; i++ {
		go c.waitCron()
	}

	for i := 0; i < 100; i++ {
		go c.workScan(i)
	}

	for i := 0; i < 200; i++ {
		go c.workSave(i)
	}
}
