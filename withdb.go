package main

import "github.com/jackc/pgx"

// Проверка существования записи о новости в базе
func (c *Config) isExist(item Item) (bool, error) {
	isExist := false
	err := c.DB.QueryRow(`SELECT count(uid)>0 FROM news WHERE id = $1`, item.GUID).Scan(&isExist)
	if err != nil {
		c.Log.Error().Err(err).Msg("Ошибка БД")
		return true, err
	}
	if isExist {
		return isExist, nil
	}
	if !isExist {
		_, err := c.DB.Exec(`INSERT INTO news (id) VALUES($1)`, item.GUID)
		if err != nil {
			c.Log.Error().Err(err).Msg("Ошибка БД")
			return true, err
		}
	}

	return false, nil
}

// Проверка ссылки по таблице проблем
func (c *Config) checkInProblems(link string) (bool, error) {
	isExist := false
	err := c.DB.QueryRow(`SELECT count(uid)>0 FROM problem WHERE link = $1 AND num > 4`, link).Scan(&isExist)
	if err == pgx.ErrNoRows {
		return false, nil
	}
	if err != nil {
		c.Log.Error().Err(err).Msg("Ошибка проверки таблицы проблем")
		return false, nil
	}
	return isExist, nil
}

// Сохранение новости в БД
func (c *Config) saveToBase(item MyItem) error {
	_, err := c.DB.Exec(`UPDATE news SET source=$2, link=$3, title=$4, description=$5, newsbody=$6, data=$7, regdata = CURRENT_TIMESTAMP  WHERE id = $1`, item.ID, item.Source, item.Link, item.Title, item.Description, item.Body, item.Date)
	if err != nil {
		c.Log.Error().Err(err).Str("Source", item.Source).Msg("Ошибка записи в БД")
	}
	return err
}

// Удаление новости из БД по идентификатору
func (c *Config) deleteFromBase(UID string) error {
	_, err := c.DB.Exec(`DELETE FROM news WHERE id = $1`, UID)
	if err != nil {
		c.Log.Error().Err(err).Str("ItemUID", UID).Msg("Ошибка удаления записи")
	}
	return err
}

func (c *Config) sendToProblem(link string) error {
	_, err := c.DB.Exec(`INSERT INTO problem (link, date) VALUES ($1, CURRENT_TIMESTAMP) ON CONFLICT (link) DO UPDATE SET num = problem.num + 1`, link)
	if err != nil {
		c.Log.Error().Err(err).Str("link", link).Msg("Ошибка добавления данных в таблицу проблем")
	}
	return err
}

// EraseData - удаление старых новостей
func (c *Config) EraseData() error {
	_, err := c.DB.Exec(`DELETE FROM news WHERE now() > news.regdata + interval '1 day'`)
	if err != nil && err != pgx.ErrNoRows {
		c.Log.Error().Err(err).Msg("Error with EraseData")
		return err
	}
	return nil
}
