package main

import (
	"encoding/xml"

	"github.com/PuerkitoBio/goquery"
)

// GetLinks - запрос списка ресурсов для парсинга
func (c *Config) GetLinks() error {
	rows, err := c.DB.Query(`SELECT uid, sourcelink, sourcetitle, titleteg, msgteg, contentteg FROM sourceslinks`)
	if err != nil {
		c.Log.Error().Err(err).Msg("Ошибка в GetLinks")
		rows.Close()
		return err
	}

	for rows.Next() {
		var source Source
		err := rows.Scan(&source.UID, &source.Sourcelink, &source.Sourcetitle, &source.TitleTeg, &source.MsgTeg, &source.ContentTeg)
		if err != nil {
			c.Log.Error().Err(err).Msg("Ошибка в GetLinks - scan")
			rows.Close()
			return err
		}

		if len(source.Sourcelink) > 0 {
			go c.SendForParsing(source)
		}
	}
	rows.Close()
	return nil
}

// SendForParsing - отправить RSS файл на парсинг
func (c *Config) SendForParsing(source Source) error {
	c.Log.Warn().Str("URL", source.Sourcelink).Msg("Читаем RSS")
	resp, err := c.httpClient.Get(source.Sourcelink)
	if err != nil {
		c.Log.Error().Err(err).Msg("Ошибка получения данных")
		return err
	}
	defer resp.Body.Close()

	var rss Rss
	decoder := xml.NewDecoder(resp.Body)
	err = decoder.Decode(&rss)
	if err != nil {
		c.Log.Error().Err(err).Msg("Ошибка декодирования RSS")
		return err
	}

	c.Log.Info().Str("Channel title", rss.Channel.Title).Msg("Парсинг")
	c.GetData(rss.Channel.Items, source)
	return nil
}

// GetData - парсинг данных
func (c *Config) GetData(items []Item, source Source) {
	for _, item := range items {
		isExist := false

		isInProblem, err := c.checkInProblems(item.Link)

		if err == nil && !isInProblem {
			isExist, err = c.isExist(item)
		}

		if !isExist && err == nil && !isInProblem {
			c.ch.parseJobs <- Element{Source: source, Item: item}
		}
	}
}

func (c *Config) workSave(num int) {
	for item := range c.ch.fetchJobs {
		if item.Error == nil && len(item.Item.ID) > 0 {
			c.saveToBase(item.Item)
			c.Log.Info().Int("Worker №", num).Msg("Ok")
		}
	}
}

func (c *Config) workScan(num int) {

	for el := range c.ch.parseJobs {

		res, err := c.httpClient.Get(el.Item.Link)
		if err != nil {
			c.Log.Warn().Int("Worker №", num).Msg("Failed")
			c.Log.Error().Err(err).Msg("Проблемы доступа")
			c.sendToProblem(el.Item.Link)
			c.deleteFromBase(el.Item.GUID)
			continue
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			c.Log.Warn().Int("Worker №", num).Msg("Canceled")
			c.Log.Error().Str("Url", el.Item.Link).Int("Статус", res.StatusCode).Msg("Проблемы доступа")
			c.sendToProblem(el.Item.Link)
			c.deleteFromBase(el.Item.GUID)
			continue
			//c.ch.fetchJobs <- FromParse{Num: num, Error: nil}
		}

		doc, err := goquery.NewDocumentFromReader(res.Body)
		if err != nil {
			c.Log.Warn().Int("Worker №", num).Msg("Canceled")
			c.Log.Error().Err(err).Str("Url", el.Item.Link).Msg("Проблемы доступа")
			c.sendToProblem(el.Item.Link)
			c.deleteFromBase(el.Item.GUID)
			continue
			// c.ch.fetchJobs <- FromParse{Num: num, Error: err}
		}

		sel := doc.Find(el.Source.ContentTeg).First()
		title := sel.Find(el.Source.TitleTeg).Text()
		text := sel.Find(el.Source.MsgTeg).Text()
		res.Body.Close()
		if len(title) == 0 || len(text) == 0 {
			c.sendToProblem(el.Item.Link)
			c.deleteFromBase(el.Item.GUID)
			c.Log.Warn().Int("Worker №", num).Msg("Canceled")
			c.Log.Error().Err(err).Str("Url", el.Item.Link).Msg("Проблема с парсингом статьи")
			continue
			// c.ch.fetchJobs <- FromParse{Num: num, Error: nil}
		}

		out := MyItem{
			ID:     el.Item.GUID,
			Source: el.Source.UID,
			Link:   el.Item.Link,
			Title:  title,
			Body:   text,
			Date:   el.Item.PubDate,
		}
		c.ch.fetchJobs <- FromParse{Num: num, Error: nil, Item: out}
	}

}
