package main

import (
	"runtime"

	"bitbucket.org/Sanny_Lebedev/test4/migration"
)

// Enclosure ...
type Enclosure struct {
	URL    string `xml:"url,attr"`
	Length int64  `xml:"length,attr"`
	Type   string `xml:"type,attr"`
}

// Item ...
type Item struct {
	Title     string    `xml:"title"`
	Link      string    `xml:"link"`
	Desc      string    `xml:"description"`
	GUID      string    `xml:"guid"`
	Enclosure Enclosure `xml:"enclosure"`
	PubDate   string    `xml:"pubDate"`
	Author    string    `xml:"author"`
}

// Channel ...
type Channel struct {
	Title string `xml:"title"`
	Link  string `xml:"link"`
	Desc  string `xml:"description"`
	Items []Item `xml:"item"`
}

// Rss ...
type Rss struct {
	Channel Channel `xml:"channel"`
}

// Close - close log file
func (c *Config) Close() {
	if c.file != nil {
		c.file.Close()
	}
}

func main() {
	// Загрузка конфигурации
	c := initial()
	defer c.Close()
	// Миграция БД по необходимости
	migration.MakeMigration(c.DB, c.Log, c.Migration)

	// Запуск каналов
	c.startChans()

	// Запуск воркеров
	c.startWorkers()

	// Запуск обработки по времени
	go c.Waitloop()
	runtime.Goexit()
}
