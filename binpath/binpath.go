package binpath

import (
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

// BinaryPath returns directory path of the executable
func BinaryPath() string {
	ex, err := os.Executable()
	if err == nil && !strings.HasPrefix(ex, "/var/folders/") && !strings.HasPrefix(ex, "/tmp/go-build") {

		return path.Dir(ex)
	}
	_, callerFile, _, _ := runtime.Caller(1)

	ex = filepath.Dir(callerFile)
	return ex
}
