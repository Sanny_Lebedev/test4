package main

import (
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"bitbucket.org/Sanny_Lebedev/test4/binpath"
	"bitbucket.org/Sanny_Lebedev/test4/logger"
	"github.com/crgimenes/goconfig"
	_ "github.com/crgimenes/goconfig/json"
	"github.com/jackc/pgx"
	"github.com/rs/zerolog/log"
)

// Инициализация БД
func pgConnect(cfg *configMain, log logger.Logger) (*pgx.ConnPool, error) {
	pgConfig := new(pgx.ConnConfig)
	pgConfig.TLSConfig = nil
	connPoolConfig := pgx.ConnPoolConfig{
		ConnConfig: pgx.ConnConfig{
			Host:     cfg.Postgres.Host,
			User:     cfg.Postgres.Username,
			Password: cfg.Postgres.Password,
			Database: cfg.Postgres.Database,
			LogLevel: pgx.LogLevelWarn,
			Logger:   log,
		},
		MaxConnections: 35,
	}
	return pgx.NewConnPool(connPoolConfig)
}

// Инициализация конфигурации
func initConfig() (*configMain, error) {
	myconfig := configMain{}
	goconfig.File = "config.json"
	goconfig.Path = binpath.BinaryPath() + "/"
	err := goconfig.Parse(&myconfig)
	if err != nil {
		log.Error().Err(err).Msg("failed to parse config file")
		return nil, err
	}
	return &myconfig, nil
}


func binaryPath() string {
	ex, err := os.Executable()
	if err == nil && !strings.HasPrefix(ex, "/var/folders/") && !strings.HasPrefix(ex, "/tmp/go-build") {
		return path.Dir(ex)
	}
	_, callerFile, _, _ := runtime.Caller(1)
	ex = filepath.Dir(callerFile)
	return ex
}
