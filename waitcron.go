package main

// Воркер обработки таймера
func (c *Config) waitCron() {
	for job := range c.ch.cronJobs {

		switch job.Action {

		case QueueActionErase:
			// Удаление лишней информации из БД
			c.EraseData()
		case QueueActionGET:
			// Старт парсинга
			c.GetLinks()
		default:
			c.Log.Error().Msg("Wrong message")
		}

	}
}
