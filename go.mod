module bitbucket.org/Sanny_Lebedev/test4

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/crgimenes/goconfig v1.2.1
	github.com/goadesign/goa v2.0.4+incompatible
	github.com/jackc/pgx v3.5.0+incompatible
	github.com/nuveo/log v0.0.0-20190430190217-44d02db6bdf8 // indirect
	github.com/rs/zerolog v1.15.0
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472
)
