package migration

import (
	"github.com/jackc/pgx"
)

// Action is a migration action
type Action interface {
	Do() error
}

// Migration is a migration item
type Migration struct {
	Action    Action
	AllowFail bool
}

// PgQuery is a postgresql query action
type PgQuery struct {
	query string
	args  []interface{}
	db    *pgx.ConnPool
}

// Fn is a function action
type Fn func() error

// Do performs an action
func (a Fn) Do() error {
	return a()
}

// NewPgQuery returns new PgQuery instance
func NewPgQuery(db *pgx.ConnPool, query string, args []interface{}) *PgQuery {
	return &PgQuery{
		query: query,
		db:    db,
		args:  args,
	}
}

// Do performs an action
func (a *PgQuery) Do() error {
	_, err := a.db.Exec(a.query, a.args...)
	return err
}
