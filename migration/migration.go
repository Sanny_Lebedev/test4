package migration

import (
	"fmt"
	"strconv"

	"bitbucket.org/Sanny_Lebedev/test4/logger"
	"github.com/jackc/pgx"
	"github.com/rs/zerolog/log"
)

func getMigration(pg *pgx.ConnPool, num int) []Migration {

	var mig = map[int][]Migration{
		0: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS configtab (
								key     varchar(25) NOT NULL DEFAULT 'currentMigration',
								val		varchar(3) NOT NULL DEFAULT '0' 
								);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS news (
					uid             uuid UNIQUE DEFAULT uuid_generate_v4(),
					id				VARCHAR(250) NOT NULL DEFAULT '',
					source			uuid NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
					link			VARCHAR(150) NOT NULL DEFAULT '',  
					title			VARCHAR(1000) NOT NULL DEFAULT '',  					
					description		VARCHAR(1000) NOT NULL DEFAULT '',	
					newsbody 		text NOT NULL DEFAULT '',				
					data        	TIMESTAMPTZ,
					regdata         timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)
				);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS title ON news (title);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS description ON news (description);`, nil),
			},
		},
		1: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS sourceslinks (
						uid uuid UNIQUE DEFAULT uuid_generate_v4(),
						sourcelink VARCHAR(1000) NOT NULL DEFAULT '',
						sourcetitle VARCHAR(150) NOT NULL DEFAULT '',
						titleteg VARCHAR(150) NOT NULL DEFAULT '',
						msgteg VARCHAR(150) NOT NULL DEFAULT '',						
						contentteg VARCHAR(150) NOT NULL DEFAULT '',
						date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3));`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS titleteg ON sourceslinks (titleteg);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS msgteg ON sourceslinks (msgteg);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS sourcelink ON sourceslinks (sourcelink);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS sourcetitle ON sourceslinks (sourcetitle);`, nil),
			},
		},
		2: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS problem (
						uid uuid UNIQUE DEFAULT uuid_generate_v4(),						
						link VARCHAR(150) UNIQUE NOT NULL DEFAULT '',						
						num numeric NOT NULL DEFAULT 0, 
						date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3));`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS link ON problem (link);`, nil),
			},
		},
	}

	return mig[num]
}

// GetMigrationfromDB - function for getting current migration number from DB
func GetMigrationfromDB(db *pgx.ConnPool) (curmignumber int, err error) {
	const sqlstr = `select val from configtab where key like 'currentMigration' `
	var val string

	err = db.QueryRow(sqlstr).Scan(&val)
	if err != nil {

		if err == pgx.ErrNoRows {
			_, err = db.Exec(`INSERT INTO configtab (key, val) VALUES('currentMigration', '0')`)
			if err != nil {
				log.Error().Err(err).Msg("Error with inserting for configtab")
				return 0, err
			}
		}
		log.Warn().Msg("No  actual currentMigration on DB")
		return 0, nil
	}

	curmignumber, err = strconv.Atoi(val)
	if err != nil {
		fmt.Println(err)
		curmignumber = 0
	}

	return curmignumber, err
}

// SetMigrationtoDB - function for setting current migration number to DB
func SetMigrationtoDB(db *pgx.ConnPool, log logger.Logger, mignum int) error {
	var sqlstr = `UPDATE configtab SET val=$1 WHERE key = $2`
	_, err := db.Exec(sqlstr, strconv.Itoa(mignum), "currentMigration")
	if err != nil {
		log.Error().Err(err).Msg("Error with SetMigrationtoDB")
	}
	return err
}

// MakeMigration - function for starting current DB migration
func MakeMigration(db *pgx.ConnPool, log logger.Logger, mig int) {
	curmignumber, err := GetMigrationfromDB(db)
	if err != nil {
		log.Error().Err(err).Msg("Error with MakeMigration")
		curmignumber = 0
	}
	if curmignumber < mig {
		curnum := curmignumber
		for curnum < mig {
			currentMigration := getMigration(db, curnum)
			for _, element := range currentMigration {
				if e := element.Action.Do(); e != nil {
					if err != nil {
						log.Error().Err(err).Msg("Error with migration")
					}
					errSetmigration := SetMigrationtoDB(db, log, (curnum))
					if errSetmigration != nil {
						log.Error().Err(errSetmigration).Msg("Error with migration")
					}
				}
			}
			curnum++
		}
		if err == nil {
			errSetmigration1 := SetMigrationtoDB(db, log, (curnum))
			if errSetmigration1 != nil {
				log.Error().Err(errSetmigration1).Msg("Error with migration")
			}
		}
	}
}
