package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"bitbucket.org/Sanny_Lebedev/test4/logger"

	"github.com/rs/zerolog"
	"golang.org/x/crypto/ssh/terminal"
)

// Инициализация конфигурации
func initial() *Config {
	var err error
	var file io.WriteCloser
	var log logger.Logger
	env := Env{}

	env.myConf, err = initConfig()
	if err != nil {
		panic(err)

	}

	if terminal.IsTerminal(int(os.Stdout.Fd())) || env.myConf.Logparam.Mode != "file" {
		log = logger.Logger{
			Logger: zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger(),
		}
	} else {
		deflogpath := "/var/log/"
		if env.myConf.Logparam.Path != "default" {
			deflogpath = env.myConf.Logparam.Path
		}

		file, err = os.OpenFile(deflogpath+"eventcron.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			panic(fmt.Sprintf("can't write log file: %v", err))
		}
		defer file.Close()
		log = logger.Logger{
			Logger: zerolog.New(file).With().Timestamp().Logger(),
		}
	}

	db, err := pgConnect(env.myConf, log)
	if err != nil {
		log.Error().Err(err).Msg("Error with DB")
	}
	log.Info().Str("Initial status", "OK").Time("Time", time.Now()).Msg("System init")

	return &Config{
		DB:        db,
		Log:       log,
		Migration: env.myConf.DBMigration.Current,
		httpClient: http.Client{
			Timeout: 100 * time.Second,
			// Transport: &http.Transport{
			// 	MaxIdleConns:       100,
			// 	IdleConnTimeout:    100 * time.Second,
			// 	DisableCompression: true,
			// },
		},
	}
}
