package main

import (
	"time"
)

//Waitloop - запуск задач по времени
func (c *Config) Waitloop() {

	ticker := time.NewTicker(time.Second * TimesForParsing)
	tickerErase := time.NewTicker(time.Minute * TimesForErasing)

	for {
		select {
		case <-tickerErase.C:
			c.ch.cronJobs <- ForWorker{Action: QueueActionErase}
		case <-ticker.C:
			c.ch.cronJobs <- ForWorker{Action: QueueActionGET}
		}
	}

}
