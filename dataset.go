package main

import (
	"io"
	"net/http"
	"time"

	"bitbucket.org/Sanny_Lebedev/test4/logger"
	"github.com/jackc/pgx"
)

type (
	postgres struct {
		Host     string `json:"host" cfg:"host" cfgDefault:"example.com"`
		Port     int    `json:"port" cfg:"port" cfgDefault:"999"`
		Password string `json:"password" cfg:"password" cfgDefault:"123"`
		Username string `json:"username" cfg:"username" cfgDefault:"username"`
		Database string `json:"database" cfg:"database" cfgDefault:"database"`
	}

	systemLog struct {
		Level   string `json:"level" cfg:"level"`
		Path    string `json:"path" cfg:"path"`
		File    string `json:"file" cfg:"file"`
		FileRes string `json:"fileres" cfg:"fileres"`
		Mode    string `json:"mode" cfg:"mode"`
	}

	configMain struct {
		Logparam    systemLog   `json:"log" cfg:"log"`
		Postgres    postgres    `json:"postgres" cfg:"postgres"`
		DBMigration dbmigration `json:"dbmigration" cfg:"dbmigration"`
	}

	dbmigration struct {
		Current int `json:"current" cfg:"current" cfgDefault:"1"`
	}

	//Config - конфигурация
	Config struct {
		DB         *pgx.ConnPool
		Log        logger.Logger
		file       io.WriteCloser
		httpClient http.Client
		tr         *http.Transport
		Migration  int
		ch         Chans
	}

	// Chans for worker
	Chans struct {
		fetchJobs chan FromParse
		parseJobs chan Element
		cronJobs  chan ForWorker
	}

	//ForWorker - structure with current job for worker
	ForWorker struct {
		Action string `json:"action"`
	}

	// FromParse - структура обработанных данных
	FromParse struct {
		URL   string `json:"action"`
		Num   int    `json:"numver"`
		Error error  `json:"error"`
		Item  MyItem
	}

	// Element ...
	Element struct {
		Item   Item
		Source Source
	}
	// ForScanning ...
	ForScanning struct {
		Element
	}

	//MyItem - Структура данных для сохранения в таблице
	MyItem struct {
		UID         string
		ID          string
		Source      string
		Title       string
		Description string
		Body        string
		Link        string
		Date        string
	}

	//Env - Структура конфигурации системы
	Env struct {
		myConf *configMain
	}

	// Source - структура для RSS ссылок новостных сайтов
	Source struct {
		UID         string    `json:"UID"`
		Sourcelink  string    `json:"sourceLink"`
		Sourcetitle string    `json:"sourceTitle"`
		ContentTeg  string    `json:"contentteg"`
		TitleTeg    string    `json:"titleTeg"`
		MsgTeg      string    `json:"msgTeg"`
		Date        time.Time `json:"date"`
	}
)

const (

	// QueueActionErase ...
	QueueActionErase = "erase"

	// QueueActionGET ...
	QueueActionGET = "Get"

	// TimesForParsing - периодичность парсинга в сек.
	TimesForParsing = 60
	// TimesForErasing - периодичность очистки БД в мин.
	TimesForErasing = 10
)
